# This migration comes from spree_simple_api (originally 20151208153644)
class AddImagelinkToWishproduct < ActiveRecord::Migration
  def up
  	add_column :spree_wished_products, :image_link ,:text
  end

  def down 	 	
  	remove_column :spree_wished_products, :image_link
  end
end
