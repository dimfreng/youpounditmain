# This migration comes from spree_simple_api (originally 20151105064347)
class ContributionNotif < ActiveRecord::Migration
  def up
    create_table   :spree_contribution_notifs do |t|
    	t.integer  :campaign_id
    	t.integer  :contribution_id
    	t.string   :status , default: 'unread'
    	t.index	   :campaign_id
    	t.index	   :contribution_id
    	t.timestamps
    end
  end

  def down
  	drop_table :spree_contribution_notifs
  end
end
