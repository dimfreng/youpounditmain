# This migration comes from spree_simple_api (originally 20150804061501)
class RenameBubby < ActiveRecord::Migration
  def up
  	rename_table :buddy_mappers ,:spree_buddy_mappers
  end

  def down
  	rename_table :spree_buddy_mappers, :buddy_mappers
  end
end
