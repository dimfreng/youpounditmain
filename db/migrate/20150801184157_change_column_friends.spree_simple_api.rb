# This migration comes from spree_simple_api (originally 20150801183727)
class ChangeColumnFriends < ActiveRecord::Migration
  def up
  	rename_column :spree_friends, :partner_id, :verification_id
  	add_index :spree_friends, :verification_id
  end

  def down
  	rename_column :spree_friends, :verification_id, :partner_id
  	remove_index :spree_friends, :verification_id
  end
end
