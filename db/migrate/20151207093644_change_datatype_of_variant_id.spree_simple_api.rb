# This migration comes from spree_simple_api (originally 20151207093416)
class ChangeDatatypeOfVariantId < ActiveRecord::Migration
   def up
  	change_column :spree_carts, :variant_id, :string
  end

  def down
  	change_column :spree_carts, :variant_id, :integer
  end
end
