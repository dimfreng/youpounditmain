# This migration comes from spree_simple_api (originally 20150803053315)
class CreateTableComment < ActiveRecord::Migration
  def up
    create_table :spree_comments do |t|
    t.integer :user_id
    t.integer :campaign_id
    t.text :body
    t.string :facebook_id
    t.string :facebook_name
    t.index :user_id
    t.index :campaign_id
    t.timestamps
    end
  end

  def down
  	drop_table :spree_comments
  end
end
