# This migration comes from spree_simple_api (originally 20150907082525)
class AddProductPath < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :product_path, :text
  end

  def down
  	remove_column :spree_campaigns, :product_path
  end
end
