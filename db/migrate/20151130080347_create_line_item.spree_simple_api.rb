# This migration comes from spree_simple_api (originally 20151130074132)
class CreateLineItem < ActiveRecord::Migration
  def up
    create_table :spree_carts do |t|
    t.integer :user_id
    t.integer :variant_id
    t.index	  :user_id
   	t.integer :quantity
   	t.string  :status , default: 'pending'
    t.timestamps
    end
  end

  def down
  	drop_table :spree_carts
  end
end
