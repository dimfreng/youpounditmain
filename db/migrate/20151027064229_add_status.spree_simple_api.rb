# This migration comes from spree_simple_api (originally 20151027063535)
class AddStatus < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :status, :string , default: 'in-progress'
  
  end

  def down
  	remove_column :spree_campaigns, :status
  end
end
