# This migration comes from spree_simple_api (originally 20150801161329)
class AddColumnCampaign < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :facebook_id, :integer
  	add_column :spree_campaigns, :facebook_name, :string
  end

  def down
  	remove_column :spree_campaigns ,:facebook_name 
  	remove_column :spree_campaigns ,:facebook_id
  end
end
