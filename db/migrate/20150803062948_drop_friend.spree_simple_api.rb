# This migration comes from spree_simple_api (originally 20150803062022)
class DropFriend < ActiveRecord::Migration
  def up
  	drop_table :spree_verifications
  	drop_table :spree_friends
  end

  def down
  	    create_table :spree_verifications do |t|
	    t.integer :user_id
	    t.index :user_id
	    end

	    create_table :spree_friends do |t|
	    t.integer :user_id
	    t.integer :verification_id
	    t.string  :status
	    t.index   :user_id	
	    t.index   :verification_id
	    end
  end
end
