# This migration comes from spree_simple_api (originally 20151208160118)
class AddTitleAndPrice < ActiveRecord::Migration
  def up
  	add_column :spree_wished_products, :title ,:text
  	add_column :spree_wished_products, :price ,:float
  	add_column :spree_carts, :title ,:text
  	add_column :spree_carts, :price ,:float
  end

  def down 	 	
  	remove_column :spree_wished_products, :title
  	remove_column :spree_wished_products, :price
  	remove_column :spree_carts, :title
  	remove_column :spree_carts, :price
  end
end
