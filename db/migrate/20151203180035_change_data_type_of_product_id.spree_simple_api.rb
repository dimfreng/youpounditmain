# This migration comes from spree_simple_api (originally 20151203175843)
class ChangeDataTypeOfProductId < ActiveRecord::Migration
  def up
  	change_column :spree_campaigns, :product_id, :string
  end

  def down
  	change_column :spree_campaigns, :product_id, :integer
  end
end
