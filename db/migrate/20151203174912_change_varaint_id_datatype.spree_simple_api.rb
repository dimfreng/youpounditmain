# This migration comes from spree_simple_api (originally 20151203174507)
class ChangeVaraintIdDatatype < ActiveRecord::Migration
  def up
  	change_column :spree_campaigns, :variant_id, :float
  end

  def down
  	change_column :spree_campaigns, :facebook_id, :integer
  end
end
