# This migration comes from spree_simple_api (originally 20150724070419)
class RenameTables < ActiveRecord::Migration
  def up
    rename_table :pledges, :spree_pledges
    rename_table :contributions, :spree_contributions
   end

	 def down
	    rename_table :spree_pledges, :pledges 
   	    rename_table :spree_contributions,:contributions
	 end
end
