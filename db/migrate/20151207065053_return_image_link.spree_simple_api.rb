# This migration comes from spree_simple_api (originally 20151207064647)
class ReturnImageLink < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns  ,:image_link ,:text
  	add_column :spree_carts, :image_link ,:text
  end

  def down 	 	
  	remove_column :spree_campaigns  ,:image_link
  	remove_column :spree_carts, :image_link
  end
end
