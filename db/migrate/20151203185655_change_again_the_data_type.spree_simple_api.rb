# This migration comes from spree_simple_api (originally 20151203183007)
class ChangeAgainTheDataType < ActiveRecord::Migration
  def up
  	remove_index :spree_campaigns, :product_id
  end

  def down
  	add_index :spree_campaigns, :product_id
  end
end
