# This migration comes from spree_simple_api (originally 20150801164813)
class CreateTableFollower < ActiveRecord::Migration
  def up
    create_table :spree_friends do |t|
    t.integer :user_id
    t.integer :partner_id
    t.string  :status
    t.index   :user_id	
    end
  end

  def down
  	drop_table :spree_friends
  end
end
