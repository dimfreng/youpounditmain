# This migration comes from spree_simple_api (originally 20150816071831)
class AddColumnProductPrice < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns, :product_price, :string
  end

  def down
  	remove_column :spree_campaigns, :product_price
  end
end
