# This migration comes from spree_simple_api (originally 20151105064332)
class CommentNotif < ActiveRecord::Migration
  def up
    create_table   :spree_comment_notifs do |t|
    	t.integer  :campaign_id
    	t.integer  :comment_id
    	t.string   :status , default: 'unread'
    	t.index	   :campaign_id
    	t.index	   :comment_id
    	t.timestamps
    end
  end

  def down
  	drop_table :spree_comment_notifs
  end
end
