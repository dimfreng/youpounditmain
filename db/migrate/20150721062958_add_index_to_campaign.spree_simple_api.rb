# This migration comes from spree_simple_api (originally 20150721062107)
class AddIndexToCampaign < ActiveRecord::Migration
  def up
  	add_index :campaigns, :user_id
  	add_index :campaigns, :product_id
  end

  def down
  	remove_index :campaigns, :user_id
  	remove_index :campaigns, :product_id
  end
end
