# This migration comes from spree_simple_api (originally 20151203175247)
class ChangeVaraintIdDatatypeAgain < ActiveRecord::Migration
   def up
  	change_column :spree_campaigns, :variant_id, :string
  end

  def down
  	change_column :spree_campaigns, :facebook_id, :float
  end
end
