# This migration comes from spree_simple_api (originally 20150806045506)
class AddAndDropTable < ActiveRecord::Migration
  def up
  	create_table :spree_friendships do |t|
    t.integer :user_id
    t.integer :friend_id
    t.string  :status
    t.timestamps
    end

    drop_table :spree_buddy_mappers
  end

  def down
  	drop_table :spree_friendships

  	create_table :spree_buddy_mappers, :force => true do |t|
      t.string :buddeable_type
      t.integer :buddeable_parent_id
      t.integer :buddeable_child_id
      t.timestamps
    end
  end
end
