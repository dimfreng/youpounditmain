# This migration comes from spree_simple_api (originally 20151105064412)
class PledgeNotif < ActiveRecord::Migration
   def up
    create_table   :spree_pledge_notifs do |t|
    	t.integer  :campaign_id
    	t.integer  :pledge_id
    	t.string   :status , default: 'unread'
    	t.index	   :campaign_id
    	t.index	   :pledge_id
    	t.timestamps
    end
  end

  def down
  	drop_table :spree_pledge_notifs
  end
end
