# This migration comes from spree_simple_api (originally 20150810073012)
class AddAddressUser < ActiveRecord::Migration
  def up
  	add_column :spree_users ,:location ,:text
  	add_column :spree_users ,:gender ,:string
  	add_column :spree_users ,:birthday ,:date
  end

  def down
  	remove_column :spree_users, :location
  	remove_column :spree_users, :gender
  	remove_column :spree_users, :birthday
  end
end
