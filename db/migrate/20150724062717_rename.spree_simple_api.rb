# This migration comes from spree_simple_api (originally 20150724062303)
class Rename < ActiveRecord::Migration
   def up
    rename_table :campaigns, :spree_campaigns
   end

	 def down
	    rename_table :spree_campaigns, :campaigns
	 end
end
