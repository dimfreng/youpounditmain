# This migration comes from spree_simple_api (originally 20151123061340)
class AddEmail < ActiveRecord::Migration
   def up
  	add_column :spree_user_authentications, :email, :string 
  end

  def down
  	remove_column :spree_user_authentications, :email
  end
end
