# This migration comes from spree_simple_api (originally 20151105070406)
class CampaiginStatusNotif < ActiveRecord::Migration
  def up
    create_table   :spree_campaign_status_notifs do |t|
    	t.integer  :campaign_id
    	t.string   :status , default: 'unread'
    	t.string   :result
    	t.index	   :campaign_id
    	t.timestamps
    end
  end

  def down
  	drop_table :spree_campaign_status_notifs
  end
end
