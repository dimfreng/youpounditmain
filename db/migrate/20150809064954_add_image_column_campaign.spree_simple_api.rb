# This migration comes from spree_simple_api (originally 20150809064713)
class AddImageColumnCampaign < ActiveRecord::Migration
  def up
  	add_column :spree_campaigns ,:image_link ,:text
  end

  def down
  	remove_column :spree_campaigns ,:image_link 
  end
end
