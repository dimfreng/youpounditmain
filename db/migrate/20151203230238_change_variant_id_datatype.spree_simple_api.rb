# This migration comes from spree_simple_api (originally 20151203225735)
class ChangeVariantIdDatatype < ActiveRecord::Migration
  def up
  	change_column :spree_wished_products, :variant_id, :string
  end

  def down
  	change_column :spree_wished_products, :variant_id, :integer
  end
end
