# This migration comes from spree_simple_api (originally 20150813030852)
class CreateTableVisitedCampaign < ActiveRecord::Migration
  def up
    create_table :spree_visited_campaigns do |t|
    	t.integer  :user_id
    	t.integer  :campaign_id
    	t.datetime :time_visited
    end
  end

  def down
  	drop_table :spree_visited_campaigns
  end
end
